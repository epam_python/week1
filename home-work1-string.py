input_dict = {
    "key1": 72,
    "key2": "intellectual",
    "key3": 'o',
    "key4": "space",
    "key5": ['Wine', 'Love', 'Corporation', 'Billboard', 'Cloud']
    }


def main():
    res1 = []
    if len(input_dict.get("key5")) % 2 != 0:
        for i in range(len(input_dict.get("key5"))):
            res1.append(input_dict.get("key5")[i][i])
    print("".join([chr(input_dict.get("key1")),
                   input_dict.get("key2")[3:6],
                   input_dict.get("key3"),
                   input_dict.get("key4").replace('space', ' '),
                   "".join(res1)]
                  )
          )


main()

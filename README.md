# week1

# Task 1
Join the results of all operations below to a string.

1)	Переведите значение ключа key1 в букву по таблице ASCII
2)	С ключа key2 возьмите срез с 4 по 6 элемент включительно
3)	Значение key3 используйте во время операции конкатенации результатов
4)	key4 замените слово на символ этого слова
5)	Создайте строку с  key5. Если в списке четное количество возьмите каждую первую букву слова. Если же кол-во нечетное, с каждого слова возьмите по букве, порядковый номер которой равен номеру этого слова в списке, например у третьего слова третью букву

 
    input_dict = {"key1": 72,
                  "key2": "intellectual",
                  "key3":'o',
                  "key4": "space",
                  "key5: ['Wine', 'Love', 'Corporation', 'Billboard', 'Cloud']}

# Task 2
Дан список arr - необходимо написать программу, которая создаст новый список, в который необходимо будет добавить элементы (dict) с возрастом от 20 лет (age), затем необходимо вывести (print)  с сортировкой по возрасту от большего к меньшему следующие значения - имя и возраст из каждого словаря, а также добавить в вывод имя в обратном порядке (задом наперед).

arr = [
    {"name": "Jake", "age": 20},
    {"name": "Mike", "age": 22},
    {"name": "Paul", "age": 10},
    {"name": "Sergey", "age": 50},
    {"name": "James", "age": 5},
]

# Task 3

Рецепт выглядит так:

РЕЦЕПТ
#========================================================
Ингридиент1                         ед.изм        кол-во
...
ИнгридиентN                         ед.изм        кол-во
#========================================================
Способ приготовления: способ
 

Написать программу, которая наполняет списки ингридиентов, единиц измерения и способов приготовления, спрашивая их по одному у пользователя (ввод с клавиатуры), пустой ввод -> окончание ввода.
Потом случайным образом выбирает ингридиенты в рецепт(без повторов и с проверкой наличия хотя бы одного ингридиента в рецепте), случайным образом выбирая единицы измерения и кол-во ингридиента в рецепте (диапазон 1..100) и формирует рецепт, как показано выше.

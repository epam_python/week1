import random


def input_list(Message):
    print(Message)
    process = True
    lst = []
    while process:
        value = input(">")
        if value == '':
            if len(lst) == 0:
                print("Ввведите хотя бы одно значение")
            else:
                process = False
        else:
            lst.append(value)
    return lst


def input_list(message):
    print(message)
    result = []
    while True:
        value = input(">")
        if not value:
            if result:
                break
            else:
                print("Ввведите хотя бы одно значение")
                continue
        result.append(value)
    return result


def main():
    cook_methods = input_list("Введите метод приготовления")
    ingredients = input_list("Введите перечень ингредиентов")
    units = input_list("Введите ед.измерения")
    used_ingredients = []
    ingredients_amount = random.randint(1, len(ingredients))
    print("Ингредиент\tед.измерения\tКол-во")
    for i in range(ingredients_amount):
        quantity = random.randint(1, 100)
        unit = units[random.randrange(len(units))]
        ingredient = ingredients[random.randrange(len(ingredients))]

        while(ingredient in used_ingredients):
            ingredient = ingredients[random.randrange(len(ingredients))]

        used_ingredients.append(ingredient)

        print("{}\t\t{}\t\t{}".format(ingredient, unit, quantity))
    cook_method = cook_methods[random.randrange(len(cook_methods))]
    print("Метод приготовления:\t\t{}".format(cook_method))


main()

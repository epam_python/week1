#!/usr/local/bin/python3

arr = [
    {"name": "Jake", "age": 20},
    {"name": "Mike", "age": 22},
    {"name": "Paul", "age": 10},
    {"name": "Sergey", "age": 50},
    {"name": "James", "age": 5},
]

# Task 1
arr1 = sorted([a for a in arr if a['age'] >= 20], key = lambda i: i['age'])

# Task 2
arr2 = sorted(arr, key = lambda i: i['age'], reverse=True)

# Task 3
for q in arr2:
    print(q['name'], q['age'], q['name'][::-1])
